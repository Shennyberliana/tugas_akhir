<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('counts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('alternatif_id')->references('id')->on('alternatifs')->onDelete('cascade'); 
            $table->foreignIdFor(Criteria::class)->nullable(); 
            $table->foreignId('subcriteria_id')->nullable()->references('id')->on('subcriterias')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('counts');
    }
}
